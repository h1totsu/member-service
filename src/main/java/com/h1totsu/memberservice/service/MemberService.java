package com.h1totsu.memberservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import com.h1totsu.memberservice.model.Member;
import com.h1totsu.memberservice.repository.MemberRepository;

@Service
public class MemberService
{
  private MemberRepository memberRepository;

  public MemberService(MemberRepository memberRepository)
  {
    this.memberRepository = memberRepository;
  }

  public Member create(Member member)
  {
    return memberRepository.insert(member);
  }

  public List<Member> findAll()
  {
    return memberRepository.findAll();
  }

  public Member update(Member member)
  {
    return memberRepository.save(member);
  }

  public Optional<Member> find(String id)
  {
    return memberRepository.findById(id);
  }

  public void delete(String id)
  {
    memberRepository.deleteById(id);
  }
}
