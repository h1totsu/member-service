package com.h1totsu.memberservice.controller;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.h1totsu.memberservice.model.Member;
import com.h1totsu.memberservice.service.MemberService;

@RestController
@RequestMapping("/members")
public class MemberController
{
  private MemberService memberService;

  public MemberController(MemberService memberService)
  {
    this.memberService = memberService;
  }

  @PostMapping
  public Member create(@RequestBody Member member)
  {
    return memberService.create(member);
  }

  @GetMapping("/{id}")
  public ResponseEntity find(@PathVariable("id") String id)
  {
    Optional<Member> noteOptional = memberService.find(id);

    return noteOptional.isPresent()
        ? ok(noteOptional.get())
        : noContent().build();
  }

  @GetMapping
  public List<Member> findAll()
  {
    return memberService.findAll();
  }

  @PutMapping
  public Member update(@RequestBody Member note)
  {
    return memberService.update(note);
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable("id") String id)
  {
    memberService.delete(id);
  }
}
