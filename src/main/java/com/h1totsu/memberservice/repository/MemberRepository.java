package com.h1totsu.memberservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.h1totsu.memberservice.model.Member;

@Repository
public interface MemberRepository extends MongoRepository<Member, String>
{
}
