FROM openjdk:8-jdk-alpine
COPY . /tmp
ARG JAR_FILE
RUN ls -als
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar", "app.jar"]